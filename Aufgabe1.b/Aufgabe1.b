

import java.util.Scanner;

public class Math {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Bitte geben Sie den Wert für x ein:");
        int x = scanner.nextInt();
        
        System.out.println("Bitte geben Sie den Wert für y ein:");
        int y = scanner.nextInt();
        
        System.out.println("Bitte geben Sie den Wert für z ein:");
        int z = scanner.nextInt();
        
        int result = fcn(x, y, z);
        System.out.println("Das Ergebnis der Funktion fcn ist: " + result);
        
        scanner.close();
    }
    
    public static int fcn(int x, int y, int z) {
        return 6 - (x/2) + (x * y) + ((z / x) * y);
    }
}


import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestMath {
    
    @Test
    public void testFcn() {
        assertEquals(25, Math.fcn(4, 3, 12));
        assertEquals(0, Math.fcn(0, 1, 2));
        assertEquals(11, Math.fcn(-4, -3, -12));
    }
}
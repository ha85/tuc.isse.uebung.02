

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestMath {
    
    @Test
    public void testFcn() {
        assertEquals(194, Math.fcn(8, 6, 24));
        assertEquals(0, Math.fcn(0, 1, 2));
        assertEquals(11, Math.fcn(-4, -3, -12));
    }
}